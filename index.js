const express = require('express');
const app = express();
const request = require('request');

//your bitcoin address
const myAddress = 'bc1q0qfzuge7vr5s2xkczrjkccmxemlyyn8mhx298v';

//interval in miliseconds
const interval = 60000;

let prevBalance = 0;

//get current balance of your address
const getBalance = (address) => {
  request('https://blockchain.info/q/addressbalance/' + address, (err, res, body) => {
    if(!err && res.statusCode === 200){
      let currentBalance = parseInt(body);
      if(prevBalance !== currentBalance){
        console.log('Your balance changed!', currentBalance);
        prevBalance = currentBalance;
      }else{
        console.log('Balance unchanged!');
      }
    }
  })
}

//set an interval to call the getBalance function
setInterval(() => {
  getBalance(myAddress);
}, interval);

//serve an html page with the current balance
app.get('/', (req, res) => {
  request('https://blockchain.info/q/addressbalance/' + myAddress, (err, response, body) => {
    if(!err && response.statusCode === 200){
      let balance = parseInt(body);
      res.send(`
        <h1>Your Current Balance is: ${balance}</h1>
        <p>This page refreshes every 1 minute.</p>
      `);
    }
  })
})

app.listen(3000, () => {
  console.log('Server listening on port 3000.');
})